<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'types'],function() {
    Route::get('/', 'TypesController@index')->name('types.index');

    Route::get('/create', 'TypesController@create')->name('types.create');
    Route::post('/create','TypesController@store')->name('types.store');

    Route::get('/edit/{id}', 'TypesController@edit')->name('types.edit');
    Route::post('/edit/{id}', 'TypesController@update')->name('types.update');

    Route::get('/show/{id}', 'TypesController@show')->name('types.show');

    Route::delete('/delete/{id}','TypesController@destroy')->name('types.delete');
});

Route::group(['prefix' => 'memes'], function(){
	Route::get('/', 'MemesController@index')->name('memes.index');

	Route::get('/create', 'MemesController@create')->name('memes.create');
	Route::post('/create', 'MemesController@store')->name('memes.store');

	Route::get('/edit/{id}', 'MemesController@edit')->name('memes.edit');
	Route::post('/edit/{id}', 'MemesController@update')->name('memes.update');

	Route::get('/show/{id}', 'MemesController@show')->name('memes.show');

	Route::delete('/delete/{id}', 'MemesController@destroy')->name('memes.delete');
});

