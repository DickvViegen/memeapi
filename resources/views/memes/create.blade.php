@extends('layouts.app')

@section('content')

<div class="container-fluid">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">
                New Meme
            </h3>
            <p class="text-center">
                Form for adding a new Meme to the DB.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <form role="form" method="POST" action="/types/create">
                {{ csrf_field() }}
                <div class="form-group">

                    <label for="name">
                        Naam meme
                    </label>
                    <input type="text" class="form-control" name="type-name" id="type-name" />
                </div>

                <div class="form-group">
                    <label for="type_id">
                        Type ID which meme belongs to.
                    </label>
                    <input type="text" class="form-control" name="type_id" id="type-id" />
                </div>
                <div class="form-group">

                    <label for="shares">
                        Shares
                    </label>
                    <input type="text" class="form-control" name="shares" id="shares" />
                </div>
                <div class="form-group">

                    <label for="img_path">
                        Meme IMG Path
                    </label>
                    <input type="text" class="form-control" name="img_path" id="img_path" />
                </div>
                <div class="form-group">

                    <label for="name">
                        Description (text in meme)
                    </label>
                    <input type="text" class="form-control" name="description" id="description" />
                </div>
                <button type="submit" class="btn btn-default">
                    Verzenden
                </button>
            </form>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>

@endsection
