@extends('layouts.app')

@section('content')

<div class="container-fluid">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">
                New Type
            </h3>
            <p class="text-center">
                Form for adding a new Meme-type to the DB.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <form role="form" method="POST" action="/types/create">
                {{ csrf_field() }}
                <div class="form-group">

                    <label for="name">
                        Naam type
                    </label>
                    <input type="text" class="form-control" name="name" id="type-name" />
                </div>

                <div class="form-group">

                    <label for="confirmed">
                        Confirmed
                    </label>
                    <input type="checkbox" name="confirmed" value=1>
                </div>
                <div class="form-group">

                    <label for="name">
                        Type IMG Path
                    </label>
                    <input type="text" class="form-control" name="img_path" id="img-path" />
                </div>
                <div class="form-group">

                    <label for="name">
                        Type Year
                    </label>
                    <input type="text" class="form-control" name="year" id="year"/>
                </div>
                <div class="form-group">

                    <label for="name">
                        Type origin
                    </label>
                    <input type="text" class="form-control" name="origin" id="type-origin" />
                </div>
                <button type="submit" class="btn btn-default">
                    Verzenden
                </button>
            </form>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>

@endsection
